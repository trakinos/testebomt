import { Component, OnInit } from '@angular/core';

import { SelfService } from '../../../services/self.service'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WarningService } from '../../../services/warnings.service'
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { CaseService } from 'src/app/services/cases.service';

@Component({
  selector: 'app-case-create',
  templateUrl: './case-create.component.html',
  styleUrls: ['./case-create.component.scss']
})
export class CaseCreateComponent implements OnInit {
  formGroup: FormGroup
  constructor(
    private selfservice:SelfService,
    private formBuilder: FormBuilder,
    private service: CaseService,
    private router: Router,
    private warn: WarningService
  ) { }

  ngOnInit(): void {
    console.log(this.selfservice.getSessionInfo())
    this.createForm()
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      status: ['pending'],
      author: [''],
      clientname: ['', [Validators.required]],
      process: ['', [Validators.required,
        Validators.pattern(/^[1-9]\d*$/)]],
      office: ['', [Validators.required,
        Validators.pattern(/^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$/)]],
      body: [''],
      estimatedVal: [0, [
        Validators.min(30000),
        Validators.pattern(/^[1-9]\d*$/)]],
      comments: [0]
    });
  }

  save() {
    if (this.formGroup.valid) {
      this.service.save(this.formGroup.value).subscribe(() => {
        this.router.navigate(['/admin/list']);
      })
      return;
    }
    this.warn.open("Os dados preenchidos não estão corretos", "Fechar")
    
  }

}
