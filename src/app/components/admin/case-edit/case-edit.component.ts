import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CaseService } from 'src/app/services/cases.service';
import { SelfService } from '../../../services/self.service'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WarningService } from '../../../services/warnings.service'
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';
@Component({
  selector: 'app-case-edit',
  templateUrl: './case-edit.component.html',
  styleUrls: ['./case-edit.component.scss']
})
export class CaseEditComponent implements OnInit {
  formGroup: FormGroup
  caseForm: boolean = false
  constructor(
    private activatedRoute: ActivatedRoute,
    private service: CaseService,
    private selfservice:SelfService,
    private formBuilder: FormBuilder,
    private router: Router,
    private warn: WarningService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(values => this.initCase(values.get('id')))
  }
  initCase(id) {
    return this.service.get(id).subscribe(data => this.createForm(data))
  }
  createForm(data) {
    this.formGroup = this.formBuilder.group({
      status: [data.status],
      author: [data.author],
      clientname: [data.clientname, [Validators.required]],
      process: [data.process, [Validators.required,
        Validators.pattern(/^[1-9]\d*$/)]],
      office: [data.office, [Validators.required,
        Validators.pattern(/^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$/)]],
      body: [data.body],
      estimatedVal: [data.estimatedVal, [
        Validators.min(30000),
        Validators.pattern(/^[1-9]\d*$/)]],
      comments: [data.comments],
      id: [data.id]
    });
    this.caseForm = true
  }
  save() {
    if (this.formGroup.valid) {
      this.service.update(this.formGroup.value).subscribe(() => {
        this.router.navigate(['/admin/list']);
      })
      return;
    }
    this.warn.open("Os dados preenchidos não estão corretos", "Fechar")
  }

}
