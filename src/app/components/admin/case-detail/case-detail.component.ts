import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { CaseService } from 'src/app/services/cases.service';

@Component({
  selector: 'app-case-detail',
  templateUrl: './case-detail.component.html',
  styleUrls: ['./case-detail.component.scss']
})
export class CaseDetailComponent implements OnInit {
  caseId
  case$
  currentCase
  constructor(
    private activatedRoute: ActivatedRoute,
    private service: CaseService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(values => {
      this.caseId = values.get('id');
      this.load(this.caseId)
      console.log(this.currentCase)
    })
  }

  load(id) {
    return this.service.get(id).subscribe(val => this.currentCase = val)
  }

  aprove() {
    return this.service.updateStatus(this.caseId, "aprovado").subscribe(() => this.load(this.caseId))
  }
  inactivate() {
    return this.service.updateStatus(this.caseId, "inativo").subscribe(() => this.load(this.caseId))
  }
}
