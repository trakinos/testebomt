
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import { AdminRoutingModule } from './admin-routes.module';
import {MatInputModule} from '@angular/material/input';

import {MatCardModule} from '@angular/material/card';


@NgModule({
  imports: [
    FormsModule,
    AdminRoutingModule,
	MatIconModule,
MatToolbarModule,
MatCardModule,
MatInputModule
  ],
  declarations: [
	
 ]
})
export class AdminModule {}
