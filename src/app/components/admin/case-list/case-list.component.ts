import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Case } from 'src/app/models';
import { CaseService } from 'src/app/services/cases.service';


@Component({
  selector: 'app-case-list',
  templateUrl: './case-list.component.html',
  styleUrls: ['./case-list.component.scss']
})
export class CaseListComponent implements OnInit {
  cases$
  displayedColumns: string[] = ['process', 'name', 'office', 'estimatedVal', 'view', 'edit'];
  constructor(
    private service: CaseService
  ) { }

  ngOnInit(): void {
    this.search()
  }

  search() {
    this.cases$ = this.service.getList()
  }

  

}
