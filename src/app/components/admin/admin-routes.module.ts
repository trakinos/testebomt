import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CaseCreateComponent } from './case-create/case-create.component';
import { CaseDetailComponent } from './case-detail/case-detail.component';
import { CaseEditComponent } from './case-edit/case-edit.component';

import { CaseListComponent } from './case-list/case-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/admin/list',
    pathMatch: 'full',
  },
  {
    path: 'admin/list',
    component: CaseListComponent,
  },
  {
    path: 'admin/case/:id',
    component: CaseDetailComponent,
  },
  {
    path: 'admin/edit/:id',
    component: CaseEditComponent,
  },
  {
    path: 'admin/create',
    component: CaseCreateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
