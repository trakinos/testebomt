import { Component, OnInit } from '@angular/core';
import { SelfService } from './../../services/self.service'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WarningService } from '../../services/warnings.service'
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup
  constructor(private selfservice:SelfService,
    private formBuilder: FormBuilder,
    private warn: WarningService,
    private router: Router
    ) { }
    
  ngOnInit(): void {
    this.createForm()
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      username: ['admin', Validators.required],
      password: ['1234', Validators.required]
    });
  }

  /**
   *  Executa o login
    * @param consumes string[] mime-types
    * @return true: consumes contains 'multipart/form-data', false: otherwise
    */
  login () {
    this.selfservice.attemptLogin(this.formGroup.value.username, this.formGroup.value.password).subscribe((data) => {
      this.verifyLogin(data)
    })
  }
  verifyLogin(data) {
    if (!!data){
      this.selfservice.sucefullLogin(data);
      this.router.navigate(['/admin']);
      return true
    }
    this.warn.open("Seu usuário ou senha estão errados", "Fechar")
    return false
  }

}
