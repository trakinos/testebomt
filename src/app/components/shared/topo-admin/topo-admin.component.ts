import { Component, OnInit } from '@angular/core';
import { SelfService } from 'src/app/services/self.service';

@Component({
  selector: 'app-topo-admin',
  templateUrl: './topo-admin.component.html',
  styleUrls: ['./topo-admin.component.scss']
})
export class TopoAdminComponent implements OnInit {

  constructor(private self: SelfService) { }

  ngOnInit(): void {
  }

  getInfo() {
    return this.self.getSessionInfo()
  }

  logout() {
    this.self.logout();
  }

}
