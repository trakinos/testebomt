import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopoAdminComponent } from './topo-admin.component';

describe('TopoAdminComponent', () => {
  let component: TopoAdminComponent;
  let fixture: ComponentFixture<TopoAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopoAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopoAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
