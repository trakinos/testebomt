import { Injectable } from '@angular/core';
import { SelfService } from '../services/self.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {

	constructor(
	  private self: SelfService,
	  private router: Router) { }
  
	canActivate(
	  route: ActivatedRouteSnapshot,
	  state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
		return this.self.isAuthenticated().pipe(tap(result => {
			if (result != true) {
				  this.router.navigate(['/login']);
				  return false
				}
				return true;
			  })
		)
  
  }
}