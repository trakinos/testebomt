import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { SelfService } from '../services/self.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(
    private self: SelfService,
    private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    // if (this.self.isAuthenticated() == true) {
    //       this.router.navigate(['/admin']);
	// 	  return false
    //     }
    //     return true;
    //   };
	return this.self.isAuthenticated().pipe(tap(result => {
		if (result == true) {
			  this.router.navigate(['/admin/list']);
			  return false
			}
			return true;
		  })
	)
		}
}
