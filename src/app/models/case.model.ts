export interface Case {
	id: Number
	office: String
	author: String
	body: String
	clientname: String
	estimatedVal: Number,
	comments: Number,
	status: String
}