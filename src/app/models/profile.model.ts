export interface Profile {
	username: String,
	password: String,
	name: String,
	id: Number
}