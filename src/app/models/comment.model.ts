export interface Comment {
	name: String
	cid: Number
	body: String
	sentTime: Date
}