import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { Layout } from './layouts/layouts.service'
import { LoginGuard } from './guards/login.guard';
import { AuthGuardService } from './guards/auth.guard';
import { CaseListComponent } from './components/admin/case-list/case-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },
  { path: 'login', component: LoginComponent},
  // { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  // { path: 'admin/lists', component: CaseListComponent},
  // Layout.blankRoutes({
  //   path: 'login', component: LoginComponent
  // }),
  // Layout.mainRoutes({
  //   path: 'admin/lists', component: LoginComponent
  // }),
  Layout.mainRoutes({
    path: 'admin',
    loadChildren: () => import('./components/admin/admin.module').then(m => m.AdminModule)
  })
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, enableTracing: false })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
