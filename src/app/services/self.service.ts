import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Profile } from '../models/index'
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
	providedIn: 'root'
  })
export class SelfService {
	private baseURL:string = "http://localhost:3000/profiles"
	
	constructor(
		protected httpClient: HttpClient,
		protected cookie: CookieService
	) {}

	/**
	 * Executa o login verificando o username e a senha entradas
     * @param username String username colocado no form do login
     * @param password String Password colocado no form de login
     * @return Observable<Profile> Profile do usuário encontrado
     */
	attemptLogin(username:String, password:String): Observable<Profile> {
		console.log(this.httpClient.get(`${this.baseURL}/?username=${username}&password=${password}`).pipe(map(data => data[0])))
		return this.httpClient.get(`${this.baseURL}/?username=${username}&password=${password}`).pipe(map(data => data[0]))
	}

	/**
	 * Cria o cookie de logado
     * @param person Profile do usuário encontrado e verificado a senha do mesmo
     */
	sucefullLogin(person:Profile) {
		let pack = JSON.stringify(person);
		this.cookie.set("AppLogged", pack);
	}

	/**
	 * Retorna o profile do usuário logado
     * @return Profile do usuário logado
     */
	getSessionInfo():Profile {
		return JSON.parse(this.cookie.get("AppLogged"));
	}

	// isAuthenticated() {
	// 	let id = this.cookie.get("AppLogged");
	// 	if (!!id) {
	// 		return true
	// 	}
	// 	return false
	// }
	public isAuthenticated() {
		return new Observable<boolean>(observable => {
			observable.next(!!this.cookie.get("AppLogged"));
		});
	  }
	
	  public logout() {
		this.cookie.delete("AppLogged");
	  }
	  
}