import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Case, Profile } from '../models/index'
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
	providedIn: 'root'
  })
export class CaseService {
	private baseURL:string = "http://localhost:3000/cases"
	private baseIdURL = "http://localhost:3000/ids"

	constructor (
		private http: HttpClient,
	) {}
	// : Observable<Case[]>
	getList() {
		return this.http.get(`${this.baseURL}`)
		// ?_sort=views&_order=asc
	}
	get(id: Number) {
		return this.http.get(`${this.baseURL}/${id}`)
	}

	incrementId(fullData) {
		return this.http.put(`${this.baseIdURL}`, fullData).pipe(map(() => fullData.cases))
	}

	getId() {
		return this.http.get(`${this.baseIdURL}`).pipe(map(data => {
			data["cases"] = data["cases"] + 1;
			this.incrementId(data).subscribe()
			return data["cases"]
		}))
	}
	save(lawCase:Case) {
		this.getId().subscribe(data => lawCase.id = data);
		return this.http.post(`${this.baseURL}`, lawCase);
	}
	update(lawCase:Case) {
		return this.http.put(`${this.baseURL}/${lawCase.id}`, lawCase);
	}

	updateStatus(id, statusVal) {
		let pack = {status: statusVal}
		return this.http.patch(`${this.baseURL}/${id}`, pack);
	}

}