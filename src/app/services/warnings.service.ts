import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class WarningService {
  constructor(private _snackBar: MatSnackBar) {}
  open(message, action) {
    this._snackBar.open(message, action, {
		duration: 2000,
	  });
  }
}
