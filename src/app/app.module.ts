import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule } from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import { LoginComponent } from './components/login/login.component'
import { SelfService } from './services/self.service';
import { CaseService } from './services/cases.service';
import { WarningService } from './services/warnings.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { CookieService } from 'ngx-cookie-service'
import { CaseListComponent } from './components/admin/case-list/case-list.component';
import { AdminModule } from './components/admin/admin.module';
import { AdminAreaComponent } from './layouts/admin-area/admin-area.component';
import { BlankComponent } from './layouts/blank/blank.component'
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import { TopoAdminComponent } from './components/shared/topo-admin/topo-admin.component';
import {MatTableModule} from '@angular/material/table';

import { CaseDetailComponent } from './components/admin/case-detail/case-detail.component';
import { CaseCreateComponent } from './components/admin/case-create/case-create.component';
import { CaseEditComponent } from './components/admin/case-edit/case-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CaseListComponent,
    AdminAreaComponent,
    BlankComponent,
    TopoAdminComponent,
    CaseDetailComponent,
CaseCreateComponent,
CaseEditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    FormsModule, ReactiveFormsModule,
    MatSnackBarModule,
    AdminModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    
  ],
  providers: [
    SelfService,
    WarningService,
    CookieService,
    CaseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
