import { Route } from '@angular/router'
import { AdminAreaComponent } from './admin-area/admin-area.component'
import { BlankComponent } from './blank/blank.component'
import { AuthGuardService } from '../guards/auth.guard';
import { LoginGuard } from '../guards/login.guard';

export class Layout {
	static mainRoutes(route: Route) {
		return Object.assign(route, {
			component: AdminAreaComponent,
			// canActivate: [AuthGuardService]
		})
	}

	static blankRoutes(route: Route) {
		return Object.assign(route, {
			component: BlankComponent,
			// canActivate: [LoginGuard]
		})
	}
}